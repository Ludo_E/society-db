--1
SELECT *
FROM employe

--2
SELECT *
FROM dept

--3
SELECT nom, dateemb, nosup, nodep, salaire
FROM employe

--4
SELECT nom, titre
FROM employe

--5
SELECT DISTINCT titre
FROM employe

--6
SELECT nom, noemp, nodep
FROM employe
WHERE titre = "secrétaire"

--7
SELECT nom, nodept
FROM dept
WHERE nodept > 40

--8
SELECT nom, prenom
FROM employe
WHERE nom < prenom

--9
SELECT nom, salaire, nodep
FROM employe
WHERE titre = "représentant" AND nodep = 35 AND salaire > 2000

--10
SELECT nom, titre, salaire
FROM employe
WHERE titre = "représentant" OR titre = "président"

--11
SELECT nom, titre, nodep, salaire
FROM employe
WHERE nodep = 34 AND (titre = "représentant" OR titre = "secrétaire")

--12
SELECT nom, salaire
FROM employe
WHERE 20000 < salaire AND salaire < 30000

--13
SELECT nom
FROM employe
WHERE nom LIKE "h%"

--14
SELECT nom
FROM employe
WHERE nom LIKE "%n"

--15
SELECT nom
FROM employe
WHERE nom LIKE "__u%"

--16
SELECT nom, salaire, nodep
FROM employe
WHERE nodep = 41
ORDER BY salaire

--17
SELECT nom, salaire, nodep
FROM employe
WHERE nodep = 41
ORDER BY salaire DESC

--18
SELECT titre, salaire, nom
FROM employe
ORDER BY titre , salaire DESC

--19
SELECT tauxcom, salaire, nom
FROM employe
ORDER BY tauxcom

--20
SELECT nom, salaire, tauxcom, titre
FROM employe
WHERE tauxcom IS NULL

--21
SELECT nom, salaire, tauxcom, titre
FROM employe
WHERE tauxcom IS NOT NULL

--22
SELECT nom, salaire, tauxcom, titre
FROM employe
WHERE tauxcom < 15

--23
SELECT nom, salaire, tauxcom, titre
FROM employe
WHERE tauxcom > 15

--24
SELECT nom, salaire, tauxcom, salaire*tauxcom/100 AS com
FROM employe
WHERE tauxcom IS NOT NULL

--25
SELECT nom, salaire, tauxcom, salaire*tauxcom/100 AS com
FROM employe
WHERE tauxcom IS NOT NULL
ORDER BY com

--26
SELECT CONCAT(nom," ", prenom) AS nomprenom
FROM employe

--27
SELECT SUBSTRING(nom, 1, 5)
FROM employe

--28
SELECT nom, LOCATE("r", nom)
FROM employe
WHERE LOCATE("r", nom) != 0

--29
SELECT nom, UPPER(nom), LOWER(nom)
FROM employe
WHERE nom = "vrante"

--30
SELECT nom, LENGTH(nom)
FROM employe