--1
SELECT prenom, noregion
FROM employe E
JOIN dept D ON E.nodep = D.nodept

--2
SELECT nodep, D.nom AS nomdepart, E.nom AS nomemploye
FROM employe E
JOIN dept D ON E.nodep = D.nodept
ORDER BY nodep

--3
SELECT E.nom, D.nom
FROM employe E
JOIN dept D ON E.nodep = D.nodept
WHERE D.nom = "distribution"

--4
SELECT A.nom, A.salaire, B.nom, B.salaire
FROM employe A
JOIN employe B ON B.noemp = A.nosup
WHERE A.salaire > B.salaire

--5
SELECT A.nom, (A.salaire+(A.tauxcom*A.salaire/100)) AS netA, B.nom, (B.salaire+(B.tauxcom*B.salaire/100)) AS netB
FROM employe A
JOIN employe B ON B.noemp = A.nosup
WHERE (A.salaire+(A.tauxcom*A.salaire/100)) > (B.salaire+(B.tauxcom*B.salaire/100))

--6
SELECT nom, titre
FROM employe
WHERE titre = ( SELECT titre FROM employe WHERE nom = "amartakaldire" )

--7
SELECT nom, salaire, nodep
FROM employe
WHERE salaire > ANY ( SELECT salaire FROM employe WHERE nodep = 31 )
ORDER BY nodep, salaire

--8
SELECT nom, salaire, nodep
FROM employe
WHERE salaire > ALL ( SELECT salaire FROM employe WHERE nodep = 31 )
ORDER BY nodep, salaire

--9
SELECT nom, titre, nodep
FROM employe
WHERE nodep = 31 AND titre IN ( SELECT titre FROM employe WHERE nodep = 32 )

--10
SELECT nom, titre, nodep
FROM employe
WHERE nodep = 31 AND titre NOT IN ( SELECT titre FROM employe WHERE nodep = 32 )

--11
SELECT nom, titre, salaire
FROM employe
WHERE titre = ( SELECT titre FROM employe WHERE nom = "fairent" ) AND salaire = ( SELECT salaire FROM employe WHERE nom = "fairent" ) AND nom != "fairent"

--12
SELECT nodept, dept.nom AS nomdepat, employe.nom AS nomsalarie
FROM dept LEFT JOIN employe ON nodept = nodep
ORDER BY nodept

--13
SELECT titre, COUNT(salaire)
FROM employe
GROUP BY titre

--14
SELECT noregion, AVG(salaire), SUM(salaire)
FROM employe JOIN dept ON nodep = nodept
GROUP BY noregion

--15
SELECT nodept
FROM employe JOIN dept ON nodep = nodept
GROUP BY nodept
HAVING COUNT(noemp) > 2

--16
SELECT SUBSTRING(nom,1,1)
FROM employe
GROUP BY SUBSTRING(nom,1,1)
HAVING COUNT(noemp) > 2

--17
SELECT MIN(salaire), MAX(salaire), MAX(salaire)-MIN(salaire) AS ecart
FROM employe

--18
SELECT COUNT(DISTINCT titre)
FROM employe

--19
SELECT titre, COUNT(noemp)
FROM employe
GROUP BY titre

--20
SELECT nodep, dept.nom, COUNT(noemp)
FROM employe JOIN dept ON nodep = nodept
GROUP BY nodep

--21
SELECT titre, AVG(salaire)
FROM employe
GROUP BY titre
HAVING AVG(salaire) > ( SELECT AVG(salaire) FROM employe WHERE titre = "représentant" )

--22
SELECT COUNT(salaire) AS nbsalaires, COUNT(tauxcom) AS nbtauxcom
FROM employe